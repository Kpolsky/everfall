extends Spatial

func _ready():
	pass
#	get_tree().connect("network_peer_connected", self, "_player_connected")

#func _player_connected(id):
#	print("Player connected to server!")
#	#Game on!
#	globals.playersConnected.append(self)
#	var game = preload("res://Game.tscn").instance()
#	get_tree().get_root().add_child(game)
#	hide()

func _on_buttonHost_pressed():

	$buttonHost.disabled = true
	$buttonHost.hide()
	$buttonJoin.disabled = true
	$buttonJoin.hide()
	
	
	network.start_server()
#	get_tree().set_network_peer(host)
#
#	print("Host connected to server!")
#	#Game on!
#	var id = get_tree().get_network_unique_id()
#	globals.playersConnected.append(id)
#	var game = preload("res://Game.tscn").instance()
#	get_tree().get_root().add_child(game)
#	hide()


func _on_buttonJoin_pressed():
#	print("Joining network")
#	var host = NetworkedMultiplayerENet.new()
#	host.create_client("127.0.0.1",4242)
	$buttonHost.disabled = true
	$buttonHost.hide()
	$buttonJoin.disabled = true
	$buttonJoin.hide()
	
	
	network.join_server()
#	get_tree().set_network_peer(host)
#
#	print("Player connected to server!")
#	#Game on!
#	var id = get_tree().get_network_unique_id()
#	globals.playersConnected.append(id)
#	var game = preload("res://Game.tscn").instance()
#	get_tree().get_root().add_child(game)
#	hide()