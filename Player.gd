extends KinematicBody

#walk() variables:
var velocity = Vector3()
var curPos = Vector3()
var run_speed = 35
var jump_speed = 100
var gravity = -250
var player_id = 0
var control = false

#remote func setPosition(position):
#	set_transform(position)

func get_input():
	velocity.x = 0
	velocity.z = 0
	var jump = Input.is_key_pressed(KEY_SPACE)
	var up = Input.is_key_pressed(KEY_W) # all of these hardwired inputs should definately be Input.is_action_pressed("ui_whatever");
	var down = Input.is_key_pressed(KEY_S)
	var left = Input.is_key_pressed(KEY_A)
	var right = Input.is_key_pressed(KEY_D)
	if is_on_floor() and jump:
		velocity.y = jump_speed
	if up:
		velocity.z -= run_speed
	if down:
		velocity.z += run_speed
	if left:
		velocity.x -= run_speed
	if right:
		velocity.x += run_speed

func _physics_process(delta):
	if(is_network_master() && control == true):
		get_input()
		velocity.y += gravity * delta
		#Move Local Player
		velocity = move_and_slide(velocity, Vector3(0, 0, 0))
		#Move Networked Position
		network.curPos = get_transform()
		#rpc_unreliable("setPosition", curPos)